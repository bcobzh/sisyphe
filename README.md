## Sisyphe 

Sisyphe is a web interface to lauch commands from GNU/Linux server or Kubernetes

it is composed by 3 other projects :

* [Janus](https://bitbucket.org/bcobzh/janus) a Javascript REST api that use CouchDB
* [Befrost](https://bitbucket.org/bcobzh/bifrost) a Vue.js/Nuxt web UI
* [Hephaistos](https://bitbucket.org/bcobzh/hephaistos) a Celery and scripts sample ( not ready yet )

### About Sisyphe name

When I'm feel confortable with the technology in which this project is writen, I rewrite it with a new technology : it's like a never ending work


### Configuration 

a http server or a k8s ingress has to be in front of Janus and Befrost 

For exemple nginx vhost should look like :

```sh
upstream befrost {
    server 127.0.0.1:3000;
    keepalive 8;
}

server {
  listen      80;
  server_name  portail.local;
  access_log /var/log/nginx/portal.local.log;

  location / {
    proxy_pass http://befrost/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }

  location /api {
    proxy_pass http://localhost:3001;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host $host;
    proxy_cache_bypass $http_upgrade;
  }
  
}
```
