cp /var/lib/rancher/k3s/agent/etc/containerd/{config.toml,config.toml.tmpl}

cat << EOF >> /var/lib/rancher/k3s/agent/etc/containerd/config.toml.tmpl

  [plugins.cri.registry.mirrors]
    [plugins.cri.registry.mirrors."docker.io"]
      endpoint = ["https://registry-1.docker.io"]
    [plugins.cri.registry.mirrors."registery.local:5000"]
      endpoint = ["https://registery.remote:5000"]
EOF
