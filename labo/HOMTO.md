# Start demo with k3os

## Install k3os
get the the ISO : https://github.com/rancher/k3os/releases/download/v0.2.1/k3os-arm64.iso


### prepare 

~~~sh
mkdir -p config/{k3,k3os}
mkdir -p ssl/
export K3OSIP=10.201.0.109
~~~

### get kube credential on desktop 

~~~sh
mkdir -p ~/.kube_configs/
scp  rancher@${K3OSIP}:/etc/rancher/k3s/k3s.yaml  ~/.kube_configs/
sed -i 's/localhost/'${K3OSIP}'/' ~/.kube_configs/k3s.yaml
sed -i 's/default/labo1/' ~/.kube_configs/k3s.yaml 
echo export KUBECONFIG=~/.kube/config:~/.kube_configs/k3s.yaml  >> ~/.bashrc
source ~/.bashrc
~~~
###  internal docker repository


#### ssl response file

~~~sh
cat << EOF > ssl/response.txt 
[ req ]
default_bits        = 2048
distinguished_name  = subject
req_extensions      = req_ext
prompt              = no


[ subject ]
C  = FR
ST = N
L  = Nantes
O  = mycompany
OU = linuxteam
CN = repository.local 

[ req_ext ]
subjectAltName = @alternate_names


[ alternate_names ]
DNS.1 = repository.local
DNS.2 = 127.0.0.1
DNS.3 = localhost.localdomain
DNS.4 = localhost
DNS.5 = 10.0.2.15
DNS.6 = 10.201.0.109

EOF

~~~


create and push 

~~~sh 
openssl genrsa -out ssl/repository.key 1024
openssl req -new -key ssl/repository.key -config ssl/response.txt -out ssl/repository.csr
openssl x509 -req -days 365 -in ssl/repository.csr -signkey ssl/repository.key  -out ssl/repository.crt
scp ssl/repository.crt rancher@${K3OSIP}:
ssh rancher@${K3OSIP} sudo mkdir -p /usr/local/share/ca-certificates/
ssh rancher@${K3OSIP} sudo mv repository.crt /usr/local/share/ca-certificates/
~~~


#### Install 

~~~
kubectl create ns repository
kubens repository
kubectl -n repository create secret tls registry-ingress-tls --cert=ssl/repository.crt --key=ssl/repository.key
~~~


~~~
sudo snap install helm --classic
echo export PATH=$PATH:/var/lib/snapd/snap/bin >> ~/.bashrc
source ~/.bashrc 

helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm install stable/docker-registry  --generate-name  --set service.type=LoadBalancer,tlsSecretName=registry-ingress-tls
~~~

#### repository without SSL
if we don’t want to use the ssl for the lab :

~~~sh
mkdir -p config/k3s/
cat <<EOS > config/k3s/patch.sh 
cp /var/lib/rancher/k3s/agent/etc/containerd/{config.toml,config.toml.tmpl}

cat << EOF >> /var/lib/rancher/k3s/agent/etc/containerd/config.toml.tmpl

  [plugins.cri.registry.mirrors]
    [plugins.cri.registry.mirrors."docker.io"]
      endpoint = ["https://registry-1.docker.io"]
    [plugins.cri.registry.mirrors."registery.local:5000"]
      endpoint = ["https://registery.remote:5000"]
EOF
EOS

scp config/k3s/patch.sh rancher@${K3OSIP}:
ssh rancher@${K3OSIP} sudo bash patch.sho bash patch.sh
~~~


####k3os persistant config
in k3os , /etc is not persistant

~~~sh 
mkdir -p config/k3os/
cat << EOF >  config/k3os/config.yaml 
run_cmd:
- "mkdir -p /etc/ca-certificates/update.d/"
- "mkdir -p /usr/local/share/ca-certificates/"
- "cp /usr/share/ca-certificates/mozilla/* /usr/local/share/ca-certificates/"
- "update-ca-certificates"
- "echo 127.0.0.1 registery.local >> /etc/hosts"
EOF

scp config/k3os/config.yaml rancher@${K3OSIP}:
ssh rancher@${K3OSIP} sudo mv config.yaml /var/lib/rancher/k3os/
~~~



#### Test

~~~sh 
echo "${K3OSIP} registery.local" >> /etc/hosts
buildah  push --tls-verify=false  registery.local:5000/myimage
~~~

##### check repo 

https://registery.local:5000/v2/_catalog

